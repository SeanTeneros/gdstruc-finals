#include <iostream>
#include <string>
#include "Stack.h"
#include "Queue.h"
#include <time.h>

using namespace std;

void main()
{
	int size;
	cout << "Enter a number for element set: ";
	cin >> size;
	size = 1;

	Queue<int> queue(size);
	Stack<int> stack(size);
	while (true)
	{
		system("cls");
		queue.top();
		stack.top();
		cout << "" << endl;
		cout << "What do you want to do?" << endl;
		cout << "1.Push Element" << endl;
		cout << "2.Pop Element" << endl;
		cout << "3.Print Element then remove" << endl;
		int choice;
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Input an Element you want to push: ";
			int input;
			cin >> input;
			queue.push(input);
			stack.push(input);

			cout << "Queue:";
			cout << queue[0]<< " " <<endl;
			cout << "Stack:" ;
			cout << stack[0]<< " " << endl;
			system("pause");

			break;

		case 2:
			queue.remove(0);
			stack.remove(0);
			cout << "Popped the Element!" << endl;

			system("pause");
			break;

		case 3:
			
			cout << "Queue: ";
			for (int i = 0; i <= queue.getSize()-1; i++)
				cout << queue[i] << "   ";
			cout << " " << endl;

			cout << "Stack: ";
			for (int i = 0; i <= stack.getSize()-1; i++)
				cout << stack[i] << "   ";
			cout << " " << endl;
			for (int i = queue.getSize(); i > 0; i--)
			{
				queue.remove(0);
			}
			for (int i = stack.getSize(); i > 0; i--)
			{
				stack.remove(0);
			}
			cout << "Printed the Elements and removed!" << endl;

			system("pause");
			break;
		}
	}
	system("pause");
}